@echo off
echo Upgrading to latest configuration, please wait...
if exist c:\Python34\python.exe (
  c:\Python34\python.exe upgrade.py
) else (
  c:\Python33\python.exe upgrade.py
)
bin\buildout.exe 2>&1 | bin\wtee -a buildout.log
pause
