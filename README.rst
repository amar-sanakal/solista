.. -*- coding: utf-8 -*-
.. :Progetto:  SoLista
.. :Creato:    lun 03 mar 2014 20:38:45 CET
.. :Autore:    Lele Gaifax <lele@metapensiero.it>
.. :Licenza:   GNU General Public License version 3 or later
..

==============================
 Easiest way to install SoL 3
==============================

----------------------------------------------
Manage Carrom_ tournaments on your own machine
----------------------------------------------

This is the easiest way to install a SoL_ instance, a buildout_ configuration that will perform
most of the needed steps with a few clicks: this is particularly indicated if you are *not*
fluent with the command line interface of your operating system.

.. note:: To perform the installation it is currently **required** to have a network
          connection, because most needed components are downloaded and installed at this
          time. A future release may provide a one-shot download format.

          Once the installation is complete the application can run even **without** the
          network connection though.

.. _Carrom: http://en.wikipedia.org/wiki/Carrom
.. _SoL: https://pypi.python.org/pypi/SoL

.. contents::


GNU/Linux installation
======================

Basic requirements
------------------

The very first requirement to install an instance of SoL on your own machine is getting Python
3.5\ [1]_ or better. On most GNU/Linux distributions it is already available\ [2]_, for example
on Debian and derivatives like Ubuntu the following command will do the task::

  $ apt-get install python3.5 python3.5-dev

To be able to produce readable PDF you need to install also the `DejaVu fonts`_. As usual, on
GNU/Linux it's a matter of executing the following command::

  $ apt-get install fonts-dejavu

or equivalent for your distribution.

Some other development libraries are needed to allow embedding `PNG` and `JPEG` images into
PDF (actually required to build the `Pillow` Python package)::

  $ apt-get install libjpeg-dev zlib1g-dev

.. _dejavu fonts: http://dejavu-fonts.org/wiki/Main_Page

Installation steps
------------------

1. Download `current version`_ of SoLista

2. Extract the content of the ZIP archive on your hard disk: it contains a single directory
   with a strange name like ``lele-solista-3ec12c59927f`` and I suggest to rename it to
   something more reasonable, like ``Sol3`` for example.

3. Execute the bootstrap

   * Open a terminal window (the `shell`) and move to the directory where you extracted the ZIP
     archive (for example with ``cd /opt/Sol3``), then execute the following command::

       $ python3.5 bootstrap.py

.. _step4linux:

4. Execute the buildout

   This step will actually download and create (or upgrade) an instance of SoL. It can be
   repeated at any time, when you want to be sure to have the latest and greatest version.

   * In a terminal window move the directory created above and issue the following command::

       $ bin/buildout

   This should take a while to complete, expecially the first time, because it downloads all
   the needed dependecies from the network.

5. Change the system passwords, or activating the `guest` user

   The first execution of the buildout produces the ``config.ini`` file containing the SoL
   configuration. It is a plain text file that you can inspect and modify with your editor of
   choice. Lines starting with the “``#``” character are `comments`.

   In particular, it contains a section like the following::

     ######################
     # SoL Authentication #
     ######################

     sol.admin.user = admin
     sol.admin.password = 52935f2ae3
     #sol.guest.user = guest
     #sol.guest.password = guest

   that determines the authentication passwords of the `admin` and the `guest` users: the
   former is the password that ``buildout`` prompted for in its first execution, the latter
   account is not activated by default. You may want to change that to something different:

     sol.admin.user = admin
     sol.admin.password = admin

   To activate the `guest` account, just remove the leading “``#``” character::

     sol.guest.user = guest
     sol.guest.password = guest

6. Launch the SoL server

   Once it's done, you should find a ``sol.sh`` file: executing this script (or *double
   clicking* it) will start the SoL application.

7. Open the application

   Start your preferred web browser\ [3]_ at the address ``http://localhost:6996/`` and enjoy!


M$-Windows installation
=======================

Basic requirements
------------------

To install Python 3.5\ [1]_ on M$-Windows you should select the right installer from the
downloads__ page on http://www.python.org/, for example the `Windows x86 executable
installer`__.

__ http://www.python.org/downloads/windows/
__ https://www.python.org/ftp/python/3.5.1/python-3.5.1.exe

To be able to produce readable PDF you need to install also the `DejaVu fonts`_. on M$-Windows
you need to download__ them and extract the archive in the right location which usually is
``C:\Windows\Fonts``.

__ http://sourceforge.net/projects/dejavu/files/dejavu/2.34/dejavu-fonts-ttf-2.34.zip

Installation steps
------------------

1. Download `current version`_ of SoLista

2. Extract the content of the ZIP archive on your hard disk: it contains a single directory
   with a strange name like ``lele-solista-3ec12c59927f`` and I suggest to rename it to
   something more reasonable, like ``Sol3`` for example. **Avoid** pathnames containing
   *spaces*, because they often cause undesirable headaches to the buildout scripts. A good bet
   is extracting the archive into something like ``C:\SoL3``.

   Within that directory you will find an handful of files, in particular:

   ``bootstrap.bat``\ [4]_
     this is the M$-Windows batch file to bootstrap the buildout

   ``bootstrap.py``
     this is the Python script needed to bootstrap the buildout

   ``buildout.bat``
     this is the M$-Windows batch file to execute the buildout

   ``buildout.cfg``
     this is the buildout configuration

3. Execute the bootstrap

   * Open the directory where you extracted the ZIP archive with the `File Manager`
     application, and *double click* on the ``bootstrap.bat`` file

.. _step4win:

4. Execute the buildout

   This step will actually download and create (or upgrade) an instance of SoL. It can be
   repeated at any time, when you want to be sure to have the latest and greatest version.

   * *Double click* on the ``buildout.bat`` file

   This should take a while to complete, expecially the first time, because it downloads all
   the needed dependecies from the network.

   The script writes its log into the ``buildout.log`` file that can be inspected (or sent to
   me) to help diagnose possible problems.

5. Change the admin password

   The first execution of the buildout produces the ``config.ini`` file containing the SoL
   configuration. It is a plain text file that you can inspect and modify with your editor of
   choice. Lines starting with the “``#``” character are `comments`.

   In particular, it contains a section like the following::

     ######################
     # SoL Authentication #
     ######################

     sol.admin.user = admin
     sol.admin.password = 52935f2ae3
     #sol.guest.user = guest
     #sol.guest.password = guest

   that determines the authentication passwords of the `admin` and the `guest` users: the
   former is the password that ``buildout`` prompted for in its first execution, the latter
   account is not activated by default. You may want to change that to something different:

     sol.admin.user = admin
     sol.admin.password = admin

   To activate the `guest` account, just remove the leading “``#``” character::

     sol.guest.user = guest
     sol.guest.password = guest

6. Launch the SoL server

   Once it's done, you should find a ``sol.bat`` file: executing this script (or *double
   clicking* it) will start the SoL application.

7. Open the application

   Start your preferred web browser\ [3]_ at the address ``http://localhost:6996/`` and enjoy!

.. _SoLista: https://bitbucket.org/lele/solista
.. _buildout: http://www.buildout.org/en/latest/
.. _current version: https://bitbucket.org/lele/solista/get/master.zip


Upgrade to the latest configuration
===================================

Now and then these instructions and more importantly the buildout configuration may change, to
fix some problems or to integrate new functionalities.

Obviously you can just repeat the process described above reinstalling SoLista from scratch in
a different directory. On the other hand, this is usually a bit wasteful since it will download
everything again. The alternative is executing the ``upgrade`` script that comes with SoLista,
that downloads the latest version of the configuration and updates only the needed files.

GNU/Linux upgrade
-----------------

Open a terminal window and move to the directory where you have the SoL instance, and execute
the following command::

  $ python3.5 upgrade.py

If nothing has changed, it will emit something like::

  Fetching https://bitbucket.org/lele/solista/get/master.zip...
  Extracting zip content...
  Nothing done, the configuration is already up-to-date!

Otherwise it will emit something like::

  Fetching https://bitbucket.org/lele/solista/get/master.zip...
  Extracting zip content...
  Backing up buildout.cfg as buildout-2014-03-22T17:45:27.cfg...
  Updating buildout.cfg...
  Done, you should re-execute the buildout now...

and as it states you should re-execute the buildout as indicated in the `fourth step`__ above.

__ step4linux_

M$Windows upgrade
-----------------

Open the directory where you extracted the ZIP archive with the `File Manager` application, and
*double click* on the ``update.bat`` file.

If nothing has changed, it will emit something like::

  Fetching https://bitbucket.org/lele/solista/get/master.zip...
  Extracting zip content...
  Nothing done, the configuration is already up-to-date!

Otherwise it will emit something like::

  Fetching https://bitbucket.org/lele/solista/get/master.zip...
  Extracting zip content...
  Backing up buildout.cfg as buildout-2014-03-22T17:45:27.cfg...
  Updating buildout.cfg...
  Done, you should re-execute the buildout now...

and as it states you should re-execute the buildout as indicated in the `fourth step`__ above.

__ step4win_


Production setup
================

If the instance is destinated to a production server, where the application is left running for
long a period of time, you may prefer using the following command at the fourth step above::

  $ bin/buildout -c production.cfg

This will create two scripts, ``start.sh`` to start the server in daemon mode and ``stop.sh``
to stop it, that may be launched by appropriate `init.d` scripts.

I'd also suggest to change the ``handler_file`` logging configuration near the end of the
``config.ini`` file to look like the following::

  [handler_file]
  class = handlers.RotatingFileHandler
  args = ('sol.log', 'a', 1000000, 5, 'utf-8')
  level = INFO
  formatter = generic

so that the log will be written to the file ``sol.log``, automatically rotated when it grows
over 1Mb.


Feedback and support
====================

If you run in troubles, or want to suggest something, or simply a desire of saying *“Thank
you”* raises up, feel free to contact me via email as ``lele at metapensiero dot it``.

Consider also joining the `dedicated mailing list`__ where you can get in contact with other
users of the application. There is also an `issues tracker`__ where you can open a new tickets
about bugs or enhancements.

__ https://groups.google.com/d/forum/sol-users
__ https://bitbucket.org/lele/sol/issues

-----

.. [#] Any version of Python **higher** than 3.3 should work, but **avoid** version 3.4.0 which
       had some issues with the ``random.random()`` function, at least on GNU/Linux...

.. [#] In fact it may even be already installed!

.. [#] I'd recommend using either `Mozilla Firefox`__ or `Google Chrome`__, but if you really
       want SoL 3 should work also on `Internet Explorer 10` or better.

.. [#] You usually won't see the ``.bat`` extension in the `File Manager` but rather you will
       see these files marked as ``Windows Batch File`` kind

__ http://www.mozilla.org/firefox/new/
__ http://www.google.com/chrome/
